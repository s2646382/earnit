package model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Student {
    private String id;
    private String name;
    private String dateOfBirth;
    private String employmentHistory;

    public Student() {

    }

    public Student(String id, String name, String dateOfBirth) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }



}
